# WordProcessor - Create & Edit Documents Online

This is basic Word Processor application. You can view a live version [here](https://t-wordprocessor.herokuapp.com/).

## Table of contents

- [Technologies](#technologies)
- [Setup](#setup)
- [Features](#features)
- [Status](#status)
- [Test suite](#test-suite)
- [CI/CD](#ci/cd)

## Technologies

### Backend

- Ruby - 2.7.0p0
- Rails - 6.0.3.4
- Database - PostgreSQL

### Frontend

- Bulma
- Javascript

### TestSuite

- RSpec
- Faker
- FactortBot

## Setup

- Clone the project on your machine using: `git clone https://gitlab.com/taiwo/word-processor.git`
- Run `bundle install` to install needed gems
- Run `rails db:setup` to setup the database
- Launch the app using `rails server`
- Visit `http://localhost:3000/` in your browser

## Features/To-do list

- Deploy live

To-do list:
 
- Allow users create a document
- Allow documents to have title
- Allow documents to have body
- Allow documents to to preview in index page

## Status

Project is: _in progress_

## Test suite

- You can run the test by running `bundle exec rspec`

## CI/CD
- To-do
