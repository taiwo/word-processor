class Document < ApplicationRecord
    has_rich_text :body
end
